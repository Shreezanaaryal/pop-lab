		PROGRAM AOFTRI
		REAL A,B,C,AREA 					!initializes the datatype of variables
		WRITE(*,*)'ENTER SIDES OF TRIANGLE'	!write the text as it is...
		READ *,A,B,C						!scan the value		
		AREA=AREAOFTRIANGLE(A,B,C)			!calls the function and put the value in AREA
		WRITE(*,*)'AREA:',AREA 				!prints the value of area of triangle
		END


		REAL FUNCTION AREAOFTRIANGLE(X,Y,Z)	!function block
		REAL X,Y,Z,S  						!initializes the data type of local variables
		S=(X+Y+Z)/2.0 						!calculates the value od semiperimeter
		AREAOFTRIANGLE=SQRT(S*(S-X)*(S-Y)*(S-Z)) !calculates the value of areaof triangle
		RETURN 										!returns the value of area of triangle
		END